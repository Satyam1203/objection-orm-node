# Objection ORM (examples)

### Steps to run and use the examples

1. Navigate to the project directory and run `npm install`.

2. All these examples use sqlite3, If you want to use MySQL, postgreSQL, etc. update it in `knexfile.js`.

3. Run `npx knex migrate:latest` which will create/update tables as defined in migrations.

4. Run `npx knex seed:run` which will run all files inside `seeds/` which are scripts to insert initial data into our tables. To run specific seeds use `knex seed:run --specific=filename.js`.

5. Run `npm run start` which will again look for migrations and update them (if any changes are encountered) and start the app at `http:localhost:3000`.

> PS: Official Repo [here](https://github.com/Vincit/objection.js).