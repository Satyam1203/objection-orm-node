const { Model } = require("objection");

class Car extends Model{
    static get tableName() {
        return 'car';
    }

    static get relationMappings() {
        const Owner = require('./Owner');
        return {
            owner: {
                modelClass: Owner,
                relation: Model.BelongsToOneRelation,
                join: {
                    from: 'car.owner_id',
                    to: 'owner.id'
                }
            }
        }
    }
}

module.exports = Car;