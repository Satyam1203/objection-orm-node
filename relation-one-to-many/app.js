const express = require('express');
const app = express();

const { Model } = require("objection");
const Knex = require("knex");

const knexfile = require("./knexfile");
const knex = Knex(knexfile[process.env.NODE_ENV || 'development']);
Model.knex(knex);

const routes = require('./routes/index');

app.use(routes);

const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log(`Listening at ${port}`)
});