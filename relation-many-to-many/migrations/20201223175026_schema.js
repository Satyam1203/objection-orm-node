
exports.up = async function(knex) {
  await knex.schema.createTable('person', (table) => {
    table.increments()
    table.string('name')
  })

  await knex.schema.createTable('restaurant', (table) => {
    table.increments()
    table.string('name')
    table.string('location')
  })

  await knex.schema.createTable('person_restaurant', (table) => {
    table.increments()
    table.integer('owner_id').unsigned().references('id').inTable('person')
    table.integer('restaurant_id').unsigned().references('id').inTable('restaurant')
  })

  return;
};

exports.down = async function(knex) {
  await knex.dropTableIfExists('person_restaurant')
  await knex.dropTableIfExists('person');
  await knex.dropTableIfExists('restaurant');
  
  return;
};
